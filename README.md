# Vuedjango

A blog like app with backend and frontend

## Getting Started

    code
$ git clone https://gitlab.com/officerebel/vuedjango.git

$ cd vuedjango

$ virtualenv vuedjango
$ source vuedjango/bin/activate

$ pip3 install django-rest_framework
$ pip3 install django-routers

### the Backend

```
Make a virtual environment
$virtualenv  django
$ source django/bin/activate

```
$ python3 manage.py runserver
```

### Activate the frontend server

```


```







## Built With

* [Django](https://www.djangoproject.com) - The web framework used


## Authors

* **Tim Vogt** - *Initial work* - [Officerebel](https://gitlab.com/officerebel)



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

